#!/usr/bin/python
# Author: Masaru Kawabata

# Import Module
import boto.ec2
import boto.rds2
import sys
import time

# Print Usage
def print_usage(args):
    print('Usage:', args[0], 'modify <profile> <region> <db name> <parameter group> <security group>')
    sys.exit(1)

# Check Region
def check_region(region_id):
    for region in boto.ec2.regions():
        if region_id == region.name:
            return 0
    return 1

# Check Arguments
def usage(args):
    # Check Argument Count
    if not len(args) == 7:
        print_usage(args)

    # Check Operation
    operation_list = ['modify']
    if not args[1] in operation_list:
        print('Error: Operation Error')
        print_usage(args)

    # Check Region Exist
    if check_region(args[3]) == 1:
        print('Error:', args[3], 'not found!')
        print_usage(args)
    return 0

# Modify RDS
def modify_rds(info):
    # Execute Modify RDS
    try:
        connrds.modify_db_instance(info['dbname'], vpc_security_group_ids=info['securitygroup'], db_parameter_group_name=info['parametergroup'], apply_immediately=True)
    except :
        print('Error: something wrong argument!')
        sys.exit(1)
    
    # Check RDS Status per 10s
    status = ''
    print('RDS Status')
    while not status == 'available':
        time.sleep(10)
        rds_list = connrds.describe_db_instances(info['dbname'])
        rds_info = rds_list['DescribeDBInstancesResponse']['DescribeDBInstancesResult']['DBInstances'][0]
        status = rds_info['DBInstanceStatus']
        print(status)

    # Execute Reboot RDS
    try:
        connrds.reboot_db_instance(info['dbname'])
    except :
        print('Error: something wrong argument!')
        sys.exit(1)
    
    # Check RDS Status per 10s
    status = ''
    print('Instance Status')
    while not status == 'available':
        time.sleep(30)
        rds_list = connrds.describe_db_instances(info['dbname'])
        rds_info = rds_list['DescribeDBInstancesResponse']['DescribeDBInstancesResult']['DBInstances'][0]
        status = rds_info['DBInstanceStatus']
        print(status)

    # Print Information
    print('---------- Instance Information ----------')
    print('DBInstance      :', rds_info['DBInstanceIdentifier'])
    print('MultiAZ         :', rds_info['MultiAZ'])
    print('DbInstanceClass :', rds_info['DBInstanceClass'])
    print('StorageSize     :', rds_info['AllocatedStorage'])
    print('Address         :', rds_info['Endpoint']['Address'])
    print('Port            :', rds_info['Endpoint']['Port'])
    print('DBUser          :', rds_info['MasterUsername'])
    print('DBName          :', rds_info['DBName'])
    print('------------------------------------------')

# Main
if __name__ == "__main__":
    # Argument Check
    usage(sys.argv)
    info = {'operation': sys.argv[1], 'profile': sys.argv[2], 'region': sys.argv[3], 'dbname': sys.argv[4], 'parametergroup': sys.argv[5], 'securitygroup': sys.argv[6]}

    # Create Connection
    connrds = boto.rds2.connect_to_region(region_name=info['region'], profile_name=info['profile'])

    # Operation RDS
    if info['operation'] == 'modify':
        modify_rds(info)

    print('RDS', info['operation'], 'Success')
    sys.exit(0)
