#!/usr/bin/python
# Author: Masaru Kawabata

# Import Module
import boto.ec2
import boto.rds2
import sys
import time

# Print Usage
def print_usage(args):
    print('Usage:', args[0], 'restore <profile> <region> <db name> <instance class> <subnet group>')
    sys.exit(1)

# Check Region
def check_region(region_id):
    for region in boto.ec2.regions():
        if region_id == region.name:
            return 0
    return 1

# Check Arguments
def usage(args):
    # Check Argument Count
    if not len(args) == 7:
        print('Error: Argument Count Error')
        print_usage(args)

    # Check Operation
    operation_list = ['restore']
    if not args[1] in operation_list:
        print('Error: Operation Error')
        print_usage(args)

    # Check Region Exist
    if check_region(args[3]) == 1:
        print('Error:', args[3], 'not found!')
        print_usage(args)
    return 0

# Get Snapshot ID
def get_latest_snapshot(dbinstance):
    db = connrds.describe_db_snapshots(dbinstance)
    snap_list = db['DescribeDBSnapshotsResponse']['DescribeDBSnapshotsResult']['DBSnapshots'] # Get Snapshot List
    snap_sort = sorted(snap_list, key=lambda x:x['InstanceCreateTime'],reverse=True)          # Sort Snapshot List Desc
    snap_id = snap_sort[0]
    return snap_id['DBSnapshotIdentifier']

# Restore RDS
def restore_rds(info):
    snapshot_id = get_latest_snapshot(info['dbname'])
    print('---------- Restore Information -----------')
    print('RestoreSnapshotID :' ,snapshot_id)
    print('------------------------------------------')

    # Execute Restore RDS
    try:
        db = connrds.restore_db_instance_from_db_snapshot(info['dbname'], snapshot_id, db_instance_class=info['dbclass'], db_subnet_group_name=info['subnetgroup'], multi_az=False)
    except :
        print('Error: something wrong argument!')
        sys.exit(1)
    
    # Check RDS Status per 10s
    status = ''
    print('RDS Status')
    while not status == 'available':
        time.sleep(30)
        rds_list = connrds.describe_db_instances(info['dbname'])
        rds_info = rds_list['DescribeDBInstancesResponse']['DescribeDBInstancesResult']['DBInstances'][0]
        status = rds_info['DBInstanceStatus']
        print(status)

    # Print Information
    print('---------- RDS Information ----------')
    print('DBInstance      :', rds_info['DBInstanceIdentifier'])
    print('MultiAZ         :', rds_info['MultiAZ'])
    print('DbInstanceClass :', rds_info['DBInstanceClass'])
    print('StorageSize     :', rds_info['AllocatedStorage'])
    print('Address         :', rds_info['Endpoint']['Address'])
    print('Port            :', rds_info['Endpoint']['Port'])
    print('DBUser          :', rds_info['MasterUsername'])
    print('DBName          :', rds_info['DBName'])
    print('-------------------------------------')

# Main
if __name__ == "__main__":
    # Argument Check
    usage(sys.argv)
    info = {'operation': sys.argv[1], 'profile': sys.argv[2], 'region': sys.argv[3], 'dbname': sys.argv[4], 'dbclass': sys.argv[5], 'subnetgroup': sys.argv[6]}

    # Create Connection
    connrds = boto.rds2.connect_to_region(region_name=info['region'], profile_name=info['profile'])

    # Operation RDS
    if info['operation'] == 'restore':
        restore_rds(info)

    print('RDS', info['operation'], 'Success')
    sys.exit(0)
