#!/usr/bin/python
# Author: Masaru Kawabata

# Import Module
import boto.ec2
import sys
import time

# Print Usage
def print_usage(args):
    print('Usage:', args[0], 'stop|start <profile> <region> <instance name>')
    sys.exit(1)

# Check Region
def check_region(region_id):
    for region in boto.ec2.regions():
        if region_id == region.name:
            return 0
    return 1

# Check Arguments
def usage(args):
    # Check Argument Count
    if not len(args) == 5:
        print('Error: Argument Count Error')
        print_usage(args)

    # Check Operation
    operation_list = ['stop', 'start']
    if not args[1] in operation_list:
        print('Error: Operation Error')
        print_usage(args)

    # Check Region Exist
    if check_region(args[3]) == 1:
        print('Error:', args[3], 'not found!')
        print_usage(args)
    return 0

# Get Instance Info
def get_instance_info(instance):
    try:
        ec2 = connec2.get_all_instances(filters={'tag:Name': instance})[0].instances[0]
    except IndexError:
        print('Error:', instance, 'not found!')
        sys.exit(1)
    return ec2

# Start Instance
def start_instance(info):
    # Get Instance Info
    ec2 = get_instance_info(info['instance'])

    if not ec2.state == 'running':
        print('Starting Instance', info['instance'])

        # Execute Start Instance
        ec2.start()

        # Check Instance Status per 10s
        print('Instance Status')
        while not ec2.state == 'running':
            time.sleep(10)
            ec2 = get_instance_info(info['instance'])
            print(ec2.state)

        # Print Information
        print('---------- Instance Information ----------')
        print('InstanceId :' ,ec2.id)
        print('PublicDNS  :' ,ec2.public_dns_name)
        print('PrivateDNS :' ,ec2.private_dns_name)
        print('------------------------------------------')
    else:
        print('Error:', info['instance'], 'already running or starting up!')
        sys.exit(1)

# Stop Instance
def stop_instance(info):
    # Get Instance Info
    ec2 = get_instance_info(info['instance'])

    if ec2.state == 'running':
        print('Stopping Instance', info['instance'])

        # Execute Stop Instance
        ec2.stop()

        # Check Instance Status per 10s
        print('Instance Status')
        while not ec2.state == 'stopped':
            time.sleep(10)
            ec2 = get_instance_info(info['instance'])
            print(ec2.state)
    else:
        print('Error:', info['instance'], 'already stopped or stopping')
        sys.exit(1)

# Main
if __name__ == "__main__":
    # Argument Check
    usage(sys.argv)
    info = {'operation': sys.argv[1], 'profile': sys.argv[2], 'region': sys.argv[3], 'instance': sys.argv[4]}

    # Create Connection
    connec2 = boto.ec2.connect_to_region(region_name=info['region'], profile_name=info['profile'])

    # Operation EC2
    if info['operation'] == 'start':
        start_instance(info)
    if info['operation'] == 'stop':
        stop_instance(info)

    print('EC2', info['operation'], 'Success')
    sys.exit(0)
