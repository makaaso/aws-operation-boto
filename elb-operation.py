#!/usr/bin/python
# Author: Masaru Kawabata

# Import Module
import boto.ec2
import boto.ec2.elb
import sys
import time

# Print Usage
def print_usage(args):
    print('Usage:', args[0], 'register|deregister <profile> <region> <elb name> <instance name>')
    sys.exit(1)

# Check Region
def check_region(region_id):
    for region in boto.ec2.regions():
        if region_id == region.name:
            return 0
    return 1

# Check Arguments
def usage(args):
    # Check Argument Count
    if not len(args) == 6:
        print('Error: Argument Count Error')
        print_usage(args)

    # Check Operation
    operation_list = ['register', 'deregister']
    if not args[1] in operation_list:
        print('Error: Operation Error')
        print_usage(args)

    # Check Region Exist
    if check_region(args[3]) == 1:
        print('Error:', args[3], 'not found!')
        print_usage(args)
    return 0

# Get Instance Info
def get_instance_id(instance):
    try:
        ec2 = connec2.get_all_instances(filters={'tag:Name': instance})[0].instances[0]
    except IndexError:
        print('Error:', instance, 'not found!')
        sys.exit(1)
    return ec2

# Register Instance
def register_instance(info):
    # Get Instance ID
    ec2 = get_instance_id(info['instance'])

    # Print Information
    print('---------- Start Register Instance ----------')
    print('ELB      :', info['elb'])
    print('Instance :', info['instance'])
    print('---------------------------------------------')

    # Execute Register Instance
    elb = connelb.register_instances(info['elb'], ec2.id)
    return 0

# Deregister Instance
def deregister_instance(info):
    # Get Instance ID
    ec2 = get_instance_id(info['instance'])

    # Print Information
    print('---------- Start Deregister Instance ----------')
    print('ELB      :', info['elb'])
    print('Instance :', info['instance'])
    print('-----------------------------------------------')

    # Execute Deregister Instance
    elb = connelb.deregister_instances(info['elb'], ec2.id)
    return 0

# Main
if __name__ == "__main__":
    # Argument Check
    usage(sys.argv)
    info = {'operation': sys.argv[1], 'profile': sys.argv[2], 'region': sys.argv[3], 'elb': sys.argv[4], 'instance': sys.argv[5]} 

    # Create Connection
    connec2 = boto.ec2.connect_to_region(region_name=info['region'], profile_name=info['profile'])
    connelb = boto.ec2.elb.connect_to_region(region_name=info['region'], profile_name=info['profile'])

    # Operation ELB
    if info['operation'] == 'register':
        register_instance(info)
    if info['operation'] == 'deregister':
        deregister_instance(info)

    print('ELB', info['operation'], 'Success')
    sys.exit(0)
