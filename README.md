### Environment
|Type       |Version |
|:---------:|:------:|
|Python     |3.4.0   |
|virtualenv |13.0.3  |

### Configration
```
~/.boto
```

### Setup
```
git clone https://makaaso@bitbucket.org/makaaso/aws-operation-boto.git
cd aws-operation-boto
virtualenv python3-env --python=/usr/local/bin/python3
source python3-env/bin/activate
pip install boto
```

