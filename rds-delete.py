#!/usr/bin/python
# Author: Masaru Kawabata

# Import Module
import boto.ec2
import boto.rds2
import datetime
import locale
import sys
import time

# Print Usage
def print_usage(args):
    print('Usage:', args[0], 'delete <profile> <region> <db name>')
    sys.exit(1)

# Check Region
def check_region(region_id):
    for region in boto.ec2.regions():
        if region_id == region.name:
            return 0
    return 1

# Check Arguments
def usage(args):
    # Check Argument Count
    if not len(args) == 5:
        print_usage(args)

    # Check Operation
    operation_list = ['delete']
    if not args[1] in operation_list:
        print('Error: Operation Error')
        print_usage(args)

    # Check Region Exist
    if check_region(args[3]) == 1:
        print('Error:', args[3], 'not found!')
        print_usage(args)
    return 0

# Delete RDS
def delete_rds(info):
    d = datetime.datetime.today()
    current_time = d.strftime("%Y%m%d%H%M%S")
    snapshot_id = info['dbname'] + '-' + current_time

    # Execute Delete RDS
    connrds.delete_db_instance(info['dbname'], skip_final_snapshot=False, final_db_snapshot_identifier=snapshot_id)

    # Check DBSnapshot Status per 10s
    status = ''
    print('Snapshot Status')
    while not status == 'available':
        time.sleep(30)
        snapshot_check = connrds.describe_db_snapshots(db_snapshot_identifier=snapshot_id)
        snapshot_info = snapshot_check['DescribeDBSnapshotsResponse']['DescribeDBSnapshotsResult']['DBSnapshots'][0]
        status = snapshot_info['Status']
        print(status, snapshot_info['PercentProgress'], '%')

    # Print Action Result
    print('---------- Snapshot Information ----------')
    print('DBSnapshot      :', snapshot_id)
    print('DBInstance      :', snapshot_info['DBInstanceIdentifier'])
    print('DBStorage       :', snapshot_info['AllocatedStorage'])
    print('------------------------------------------')

# Main
if __name__ == "__main__":
    # Argument Check
    usage(sys.argv)
    info = {'operation': sys.argv[1], 'profile': sys.argv[2], 'region': sys.argv[3], 'dbname': sys.argv[4]}

    # Create Connection
    connrds = boto.rds2.connect_to_region(region_name=info['region'], profile_name=info['profile'])

    # Operation RDS
    if info['operation'] == 'delete':
        delete_rds(info)

    print('RDS', info['operation'], 'Success')
    sys.exit(0)
